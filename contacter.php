<!DOCTYPE HTML>
<!--
	Theory by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Nos Produits</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/scss/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="index.html" class="logo">Acceuil</a>
					<nav id="nav" >
						<a href="service.html">Nos Services</a>
						<a href="produit.html">Nos Produits</a>
						<a href="tutoriel.php">Tutoriels</a>
                        <a href="contacter.php">Nous-contacter</a>
					</nav>
					<a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
				</div>
			</header>

		<!-- Main -->
			<section id="three" class="wrapper special">
				<div class="inner">
					<header class="align-center">
						<h2>NOUS-CONTACTER</h2>
						<p>Vous pouves nous contacter en fonction de l'un des ses informations ci-dessous </p>
					</header>
                    <h4>Informations</h4>
                    <ul class="icons">
											<li><a href="#" class="icon fa-map-marker"><span class="label">Adresse</span> 514, Delmas 66 (BANJ)</a></li>
											<li><a href="#" class="icon fa-phone"><span class="label">Téléphone</span>  (+509) 4301-1736 /3763-0065 / 4241-9494 / 3243-9955</a></li>
											<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span> softmatech.ht@gmail.com</a></li>
											
										</ul>
                    <hr />
					<h3>Laisses-nous un message</h3>

								<form method="post" action="#">
									<div class="row uniform">
										<div class="6u 12u$(xsmall)">
											<input type="text" name="name" id="name" value="" placeholder="Nom Complet" />
										</div>
										<div class="6u$ 12u$(xsmall)">
											<input type="email" name="email" id="email" value="" placeholder="Email" />
										</div>
										<!-- Break -->
										<div class="6u 12u$(xsmall)">
											<input type="text" name="name" id="name" value="" placeholder="Téléphone" />
										</div>
                                        <div class="6u 12u$(xsmall)">
											<input type="text" name="name" id="name" value="" placeholder="Objet du Message" />
										</div>
										<!-- Break -->
										<div class="12u$">
											<textarea name="message" id="message" placeholder="Tapez votre message" rows="6"></textarea>
										</div>
										<!-- Break -->
										<div class="12u$">
											<ul class="actions">
												<li><input type="submit" class="button special fit" value="Send Message"  /></li>
												
											</ul>
										</div>
									</div>
								</form>

				</div>
			</section>

		<!-- Footer -->
			<footer id="footer">
				<div class="inner">
					<div class="flex">
						<div class="copyright">
							&copy; SOFTMATECH 
						</div>
						<ul class="icons">
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-youtube"><span class="label">Youtube</span></a></li>
							<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
							
						</ul>
					</div>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>