<!DOCTYPE HTML>
<!--
	Theory by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Tutoriels</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/scss/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="index.html" class="logo">Acceuil</a>
					<nav id="nav" >
						<a href="service.html">Nos Services</a>
						<a href="produit.html">Nos Produits</a>
						<a href="tutoriel.php">Tutoriels</a>
                        <a href="contacter.php">Nous-contacter</a>
					</nav>
					<a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
				</div>
			</header>

		<!-- Main -->
			<section id="three" class="wrapper special">
				<div class="inner">
					<header class="align-center">
						<h2>TUTORIELS</h2>
						<p>Nous Offrons des services hors norme </p>
					</header>

								<form method="post" action="#">
									
                                    <h3>Tutoriel</h3>
								<span class="image fit"><img src="images/pic01.jpg" alt="" /></span>
								<div class="box alt">
									<div class="row 50% uniform">
										<div class="4u"><span class="image fit"><img src="images/pic01.jpg" alt="" /></span></div>
										<div class="4u"><span class="image fit"><img src="images/pic01.jpg" alt="" /></span></div>
										<div class="4u$"><span class="image fit"><img src="images/pic01.jpg" alt="" /></span></div>
										<!-- Break -->
										<div class="4u"><span class="image fit"><img src="images/pic01.jpg" alt="" /></span></div>
										<div class="4u"><span class="image fit"><img src="images/pic01.jpg" alt="" /></span></div>
										<div class="4u$"><span class="image fit"><img src="images/pic01.jpg" alt="" /></span></div>
										<!-- Break -->
										<div class="4u"><span class="image fit"><img src="images/pic01.jpg" alt="" /></span></div>
										<div class="4u"><span class="image fit"><img src="images/pic01.jpg" alt="" /></span></div>
										<div class="4u$"><span class="image fit"><img src="images/pic01.jpg" alt="" /></span></div>
									</div>
								</div>
								</form>

				</div>
			</section>

		<!-- Footer -->
			<footer id="footer">
				<div class="inner">
					<div class="flex">
						<div class="copyright">
							&copy; SOFTMATECH 
						</div>
						<ul class="icons">
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-youtube"><span class="label">Youtube</span></a></li>
							<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
							
						</ul>
					</div>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>